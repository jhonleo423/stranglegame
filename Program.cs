﻿namespace  StrangleGame
{
    class Program
    {
        static void Main()
        {
            string value = "";
            do
            {
                //APLICAR EL COLOR DE LA FUENTE
                Console.WriteLine("");
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("===============AHORCADO EN C#===============");
                if(value == "--")
                {
                    Console.WriteLine("");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("Ingrese una opcion valida");

                };
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("");
                Console.WriteLine("Opciones del juego: ");
                Console.WriteLine("1) Jugar partida");
                Console.WriteLine("2) Mostar información del autor");
                Console.WriteLine("X) Salir de juego");

                Console.WriteLine("");
                Console.Write("Seleccione la opcion del juego: ");
                value = String.Format(Console.ReadLine());
                Console.WriteLine("");
                switch(value)
                {
                    case "1":
                        Console.WriteLine("Vamos a jugar");
                        Game game = new Game();
                        break;
                    case "2":
                        Console.WriteLine("Jhon Leon (cleon@corrosioncic.com)");
                        break;
                    case "x":
                    case "X":
                        Console.WriteLine("Salir");
                        break;
                    default:
                        Console.WriteLine("Opción no valida");
                        value = "--";
                        break;
                }
            }while(value == "--");
        }
    }
}


