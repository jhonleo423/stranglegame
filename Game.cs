namespace  StrangleGame
{
    class Game
    {
        //Vamos a añadir las propiedades del juego ahorcado
        // Intentos necesarios
        public int Attemps {get; set;}
        //Palabra secreta
        public string HideWord {get; set;}
        // palabra oculta "encriptada"
        public string GameWordChardsShow {get; set;}

        //Listas para el flujo de juego
        public List<char> InputCharsList {get; set;}
        public List<char> HideWordChars {get; set;}
        public List<char> CorrectChars {get; set ;}

        public Game()
        {
            //Intentos por defecto 6
            Attemps = 6;
            //Anadimos la palabra oculta fija
            HideWord = GetHideWord();
            //Convertir el string en un array de carracteres para aplicar las listas necesarias
            char [] chartListElements = HideWord.ToLower().ToCharArray();
            //Inicializar lista para los carracteres que vamos introduciendo
            InputCharsList = new List<char>();

            HideWordChars = new List<char>(chartListElements);

            CorrectChars = new List<char>(chartListElements);

            for(int i = 0; i < HideWordChars.Count; i++)
            {
                if(HideWordChars[i] != ' ')
                {
                    HideWordChars[i] = '_';
                    GameWordChardsShow += "_ ";
                }else{
                    GameWordChardsShow += "  ";
                };
            }
            DrawGameImage();
            Console.WriteLine("Palabra a buscar: ");
            Console.WriteLine(GameWordChardsShow);
            Play();
        }

        public void Play()
        {
            //Mientras jugamos
            while(Attemps > 0 && HideWordChars.Contains('_')){
                //Console.WriteLine("Intentos: {0}", Attemps);
                //Introducir el carrater desde la consola con el teclado
                char inputChar = ' ';
                Console.Write("\nIntroduzca la letra: ");
                try{
                    inputChar = Console.ReadLine().ToLower() [0];
                } catch(IndexOutOfRangeException){
                    Console.WriteLine("Debes ingresar letra");
                } catch(Exception e){
                    Console.WriteLine("Error general {0}", e);
                };
                //Comprobar que es un carácter válido
                if(inputChar >= 'a' && inputChar <= 'z'){
                    Console.WriteLine("Carracter válido");
                    //Comprobar si el caracter se ha introducido
                    if(!InputCharsList.Contains(inputChar)){
                        Console.Clear();
                        //Aladir para no repetir palabra oculta
                        InputCharsList.Add(inputChar);
                        //Comprobar si exite en la palabra oculta
                        CheckExistCharInWord(inputChar);
                        //Dibujar el resultado dependiendo del resultado dado en la comprobación
                        DrawGameImage();
                        Console.WriteLine("Palabra a buscar: ");
                        Console.WriteLine(GameWordChardsShow);
                    } else{
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine("Ya has introducido el caracter '{0}'. Ingresa una nueva linea por favor", inputChar);
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                }
            }
            //Partida finalizada
            if(Attemps == 0)
            {
                DrawGameImage();
            }else if(!HideWordChars.Contains('-')){
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Bien hecho, has ganado");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        public string GetHideWord()
        {
            List<string> hidenWord = LoadWordChars();
            Random random = new Random();
            int numberRandom = random.Next(0, hidenWord.Count);
            return hidenWord[numberRandom];
        }
        private List<string> LoadWordChars()
        {
            string loadText = File.ReadAllText("./data/sagas-miticas.txt");
            string [] words = loadText.Split("\n");
            return new List<string>(words);
        }
        private void CheckExistCharInWord(char inputChar)
        {
            //Comprobar que existe dentro de CorrectChars
            if(CorrectChars.Contains(inputChar))
            {
                Console.WriteLine("Correcto!");
                GameWordChardsShow = "";
                for(int i = 0; i < HideWordChars.Count; i++)
                {
                    if(CorrectChars[i] ==  inputChar)
                    {
                        HideWordChars[i] = inputChar;
                    }
                    GameWordChardsShow += (HideWordChars[i] != ' ') ? HideWordChars[i] + " ": "   ";
                }
            }else
            {
                //No ha acertado
                Attemps--;
                Console.WriteLine("Incorrecto");
            }
        }
        private void DrawGameImage()
        {
            Console.WriteLine("====================");
            Console.WriteLine($"Intentos: {Attemps}");
            Console.WriteLine("====================");
            switch (Attemps)
            {
                case 6:
                    Console.WriteLine(" ---------------------");
                    for (int j = 0; j <= 15; j++)
                    {
                        Console.WriteLine(" |");

                    }
                    Console.WriteLine("__________");
                    break;

                case 5:
                    Console.WriteLine(" ---------------------");
                    Console.WriteLine(" |                     |");
                    Console.WriteLine(" |                     |");
                    Console.WriteLine(" |                  -------");
                    Console.WriteLine(" |                 | -  -  |");
                    Console.WriteLine(" |                 |   o   |");
                    Console.WriteLine(" |                  -------");
                    for (int j = 0; j <= 10; j++)
                    {
                        Console.WriteLine(" |");

                    }
                    Console.WriteLine("__________");
                    break;

                case 4:
                    Console.WriteLine(" ---------------------");
                    Console.WriteLine(" |                     |");
                    Console.WriteLine(" |                     |");
                    Console.WriteLine(" |                  -------");
                    Console.WriteLine(" |                 | -  -  |");
                    Console.WriteLine(" |                 |   o   |");
                    Console.WriteLine(" |                  -------");
                    Console.WriteLine(" |                     |   ");
                    Console.WriteLine(" |                     |   ");
                    Console.WriteLine(" |                     |   ");
                    Console.WriteLine(" |                     |   ");
                    Console.WriteLine(" |                     |   ");
                    for (int j = 0; j <= 5; j++)
                    {
                        Console.WriteLine(" |");

                    }
                    Console.WriteLine("__________");
                    break;

                case 3:
                    Console.WriteLine(" ---------------------");
                    Console.WriteLine(" |                     |");
                    Console.WriteLine(" |                     |");
                    Console.WriteLine(" |                  -------");
                    Console.WriteLine(" |                 | -  -  |");
                    Console.WriteLine(" |                 |   o   |");
                    Console.WriteLine(" |                  -------");
                    Console.WriteLine(" |                     |   ");
                    Console.WriteLine(" |                   / |   ");
                    Console.WriteLine(" |                  /  |   ");
                    Console.WriteLine(" |                 /   |   ");
                    Console.WriteLine(" |                     |   ");
                    for (int j = 0; j <= 5; j++)
                    {
                        Console.WriteLine(" |");

                    }
                    Console.WriteLine("__________");
                    break;

                case 2:
                    Console.WriteLine(" ---------------------");
                    Console.WriteLine(" |                     |");
                    Console.WriteLine(" |                     |");
                    Console.WriteLine(" |                  -------");
                    Console.WriteLine(" |                 | -  -  |");
                    Console.WriteLine(" |                 |   o   |");
                    Console.WriteLine(" |                  -------");
                    Console.WriteLine(" |                     |   ");
                    Console.WriteLine(" |                   / | \\ ");
                    Console.WriteLine(" |                  /  |   \\ ");
                    Console.WriteLine(" |                 /   |     \\ ");
                    Console.WriteLine(" |                     |   ");
                    for (int j = 0; j <= 5; j++)
                    {
                        Console.WriteLine(" |");

                    }
                    Console.WriteLine("__________");
                    break;

                case 1:
                    Console.WriteLine(" ---------------------");
                    Console.WriteLine(" |                     |");
                    Console.WriteLine(" |                     |");
                    Console.WriteLine(" |                  -------");
                    Console.WriteLine(" |                 | -  -  |");
                    Console.WriteLine(" |                 |   o   |");
                    Console.WriteLine(" |                  -------");
                    Console.WriteLine(" |                     |   ");
                    Console.WriteLine(" |                   / | \\ ");
                    Console.WriteLine(" |                  /  |   \\ ");
                    Console.WriteLine(" |                 /   |     \\ ");
                    Console.WriteLine(" |                     |   ");
                    Console.WriteLine(" |                    /  ");
                    Console.WriteLine(" |                   /      ");
                    Console.WriteLine(" |                  /       ");
                    for (int j = 0; j <= 2; j++)
                    {
                        Console.WriteLine(" |");

                    }
                    Console.WriteLine("__________");
                    break;

                case 0:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"GAME OVER - La palabra oculta es : \"{HideWord}\"");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(" ---------------------");
                    Console.WriteLine(" |                     |");
                    Console.WriteLine(" |                     |");
                    Console.WriteLine(" |                  -------");
                    Console.WriteLine(" |                 | X  X  |");
                    Console.WriteLine(" |                 |   o   |");
                    Console.WriteLine(" |                  -------");
                    Console.WriteLine(" |                     |   ");
                    Console.WriteLine(" |                   / | \\ ");
                    Console.WriteLine(" |                  /  |   \\ ");
                    Console.WriteLine(" |                 /   |     \\ ");
                    Console.WriteLine(" |                     |   ");
                    Console.WriteLine(" |                    / \\");
                    Console.WriteLine(" |                   /   \\  ");
                    Console.WriteLine(" |                  /     \\ ");
                    for (int j = 0; j <= 2; j++)
                    {
                        Console.WriteLine(" |");

                    }
                    Console.WriteLine("__________");
                    break;
            }
        }
    }
}


